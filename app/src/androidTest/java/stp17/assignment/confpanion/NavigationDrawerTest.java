package stp17.assignment.confpanion;

import android.view.Gravity;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import stp17.assignment.confpanion.ui.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.contrib.DrawerMatchers.isOpen;

    @RunWith(AndroidJUnit4.class)
    @LargeTest
    public class NavigationDrawerTest {

        @Rule
        public ActivityTestRule<MainActivity> activityActivityTestRule =
                new ActivityTestRule<>(MainActivity.class);

        @Test
        public void clickOnAndroidHomeIcon_OpensNavigation() {
            // Check that left drawer is closed at startup
            onView(withId(R.id.drawer_layout))
                    .check(matches(isClosed(Gravity.LEFT))); // Left Drawer should be closed.

            // Open Drawer
            String navigateUpDesc = activityActivityTestRule.getActivity()
                    .getString(R.string.nav_open_drawer);
            onView(withContentDescription(navigateUpDesc)).perform(click());

            // Check if drawer is open
            onView(withId(R.id.drawer_layout))
                    .check(matches(isOpen(Gravity.LEFT))); // Left drawer is open open.
        }
    }