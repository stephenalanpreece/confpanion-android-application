package stp17.assignment.confpanion.locations;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

import androidx.annotation.Nullable;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;

/**
 * The popup screen the users sees when a session is clicked,
 * the data is being brought in through intents using parcelable data
 * @Author Stephen Preece
 * @Version 1
 * @Date 27/11/2019
 */


public class LocationInfoPopUp extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.locationinfopopup);

        //set text view variables to XML text views
        TextView locationDescTextView = findViewById(R.id.descriptionText);
        TextView locationLongitudeTextView = findViewById(R.id.longitudeText);
        TextView locationLatitudeTextView = findViewById(R.id.latitudeText);


        //unpack parcel and create new Session object using passed parcelable data
        Intent intent = getIntent();
        Location locationInfo = intent.getParcelableExtra("Location Info");

        //set the session info variables
        String locationDesc = locationInfo.getDescription();
        float locationLatitude = locationInfo.getLatitude();
        float locationLongitude = locationInfo.getLongitude();

        //bind data to the text views
        locationDescTextView.setText(locationDesc);
        locationLongitudeTextView.setText(String.valueOf(locationLongitude));
        locationLatitudeTextView.setText(String.valueOf(locationLatitude));

        // set the activity screen size
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .55));


    }
}
