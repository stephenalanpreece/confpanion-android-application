package stp17.assignment.confpanion.locations;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

/**
 *
 * A data access object for the location table
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */
@Dao
public interface LocationDao {

    @Query("SELECT * FROM locations ORDER BY name")
    LiveData<List<Location>> getAllLocations();
}
