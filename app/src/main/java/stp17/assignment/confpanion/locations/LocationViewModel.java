package stp17.assignment.confpanion.locations;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import stp17.assignment.confpanion.datasource.ConfRepository;

/**
 * A View Model that handles the location data that needs to be stored in the UI
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

public class LocationViewModel extends AndroidViewModel {

    private ConfRepository repository;
    private LiveData<List<Location>> locationList;

    private String locationName;
    private String locationId;

    private LocationsRecyclerWithListAdapter adapter;

    public LocationViewModel(@NonNull Application application) {
        super(application);
        repository = new ConfRepository(application);

        //load in all locations from database
        loadLocations();
    }

    public LiveData<List<Location>> getLocations() {
        return locationList;
    }

    public LocationsRecyclerWithListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LocationsRecyclerWithListAdapter adapter) {
        this.adapter = adapter;
    }

    private void loadLocations() {
        locationList = repository.getAllLocations();
    }

    public LiveData<List<Location>> getLocationList() {
        return locationList;
    }

    public ConfRepository getRepository() {
        return repository;
    }

    public void setRepository(ConfRepository repository) {
        this.repository = repository;
    }


    public void setLocationList(LiveData<List<Location>> locationList) {
        this.locationList = locationList;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
}
