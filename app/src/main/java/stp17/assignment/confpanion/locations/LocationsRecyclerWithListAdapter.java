package stp17.assignment.confpanion.locations;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import stp17.assignment.confpanion.R;

/**
 * A basic adapter class that is able to use Livedata to be fed into the UI
 * This one takes Locations IMPORTANT: Did not comment each method as it's template code
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

public class LocationsRecyclerWithListAdapter extends RecyclerView.Adapter<LocationsRecyclerWithListAdapter.ViewHolder> {
    private final Context context;
    private List<Location> dataSet;
    private LocationsRecyclerWithListAdapter.OnLocationClickListener onLocationClickListener;

    public LocationsRecyclerWithListAdapter(Context context, OnLocationClickListener onLocationClickListener) {
        this.context = context;
        this.onLocationClickListener = onLocationClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView locationName;

        LocationsRecyclerWithListAdapter.OnLocationClickListener onLocationClickListener;

        ViewHolder(View itemView, LocationsRecyclerWithListAdapter.OnLocationClickListener onLocationClickListener) {
            super(itemView);

            locationName = itemView.findViewById(R.id.locationName);

            this.onLocationClickListener = onLocationClickListener;

            itemView.setOnClickListener(this);
        }

        void bindDataSet(Location location) {
            locationName.setText(location.getName());
        }

        @Override
        public void onClick(View v) {
            onLocationClickListener.onLocationClick(getAdapterPosition());

            //bring the parcelable data from the clicked object
            Intent intent = new Intent(context, LocationInfoPopUp.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Location Info", dataSet.get(getAdapterPosition()));

            context.startActivity(intent);
        }

    }

    @NonNull
    @Override
    public LocationsRecyclerWithListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.recycler_view_location_item, parent, false);
        return new ViewHolder(view, onLocationClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationsRecyclerWithListAdapter.ViewHolder holder, int position) {
        if (dataSet != null) {
            holder.bindDataSet(dataSet.get(position));
        }

    }

    @Override
    public int getItemCount() {
        if (dataSet != null) {
            return dataSet.size();
        } else {
            return 0;
        }
    }

    public void changeDataSet(List<Location> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    /**
     * Sets up an interface to interact with onClick events
     */
    public interface OnLocationClickListener {
        void onLocationClick(int position);
    }


}
