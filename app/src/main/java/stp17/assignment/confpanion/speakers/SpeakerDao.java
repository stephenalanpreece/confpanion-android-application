package stp17.assignment.confpanion.speakers;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import stp17.assignment.confpanion.sessions.Session;

/**
 *
 * A data access object for the speakers table
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

@Dao
public interface SpeakerDao {

    @Query("SELECT * FROM speakers ORDER BY name")
    LiveData<List<Speaker>> getAllSpeakers();
}