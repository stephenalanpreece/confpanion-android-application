package stp17.assignment.confpanion.speakers;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import stp17.assignment.confpanion.datasource.ConfRepository;

/**
 * A View Model that handles the speaker data that needs to be stored in the UI
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

public class SpeakerViewModel extends AndroidViewModel {

    private ConfRepository repository;
    private LiveData<List<Speaker>> speakerList;

    private String speakerName;
    private String speakerId;

    private SpeakersRecyclerWithListAdapter adapter;

    public SpeakerViewModel(@NonNull Application application) {
        super(application);
        repository = new ConfRepository(application);

        loadSpeakers();
    }

    public LiveData<List<Speaker>> getSpeakers() {
        return speakerList;
    }

    public SpeakersRecyclerWithListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(SpeakersRecyclerWithListAdapter adapter) {
        this.adapter = adapter;
    }

    private void loadSpeakers() {
        speakerList = repository.getAllSpeakers();
    }

    public LiveData<List<Speaker>> getSpeakerList() {
        return speakerList;
    }

    public ConfRepository getRepository() {
        return repository;
    }

    public void setRepository(ConfRepository repository) {
        this.repository = repository;
    }


    public void setSpeakerList(LiveData<List<Speaker>> speakerList) {
        this.speakerList = speakerList;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public String getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(String speakerId) {
        this.speakerId = speakerId;
    }
}
