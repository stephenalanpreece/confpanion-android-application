package stp17.assignment.confpanion.speakers;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * A parcelable class for each speaker used at the conference
 * Also acts as an entity for the relevant table
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

@Entity(tableName = "speakers")
public class Speaker implements Parcelable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "biography")
    private String biography;
    @ColumnInfo(name = "twitter")
    private String twitter;

    public Speaker(String id, String name, String biography, String twitter) {
        this.id = id;
        this.name = name;
        this.biography = biography;
        this.twitter = twitter;
    }

    protected Speaker(Parcel in) {
        id = in.readString();
        name = in.readString();
        biography = in.readString();
        twitter = in.readString();
    }

    public static final Creator<Speaker> CREATOR = new Creator<Speaker>() {
        @Override
        public Speaker createFromParcel(Parcel in) {
            return new Speaker(in);
        }

        @Override
        public Speaker[] newArray(int size) {
            return new Speaker[size];
        }
    };

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(biography);
        dest.writeString(twitter);
    }
}
