package stp17.assignment.confpanion.speakers;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.ui.Locations;

/**
 * A basic adapter class that is able to use Livedata to be fed into the UI
 * This one takes Speakers
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

public class SpeakersRecyclerWithListAdapter extends RecyclerView.Adapter<SpeakersRecyclerWithListAdapter.ViewHolder> {
    private final Context context;
    private List<Speaker> dataSet;
    private SpeakersRecyclerWithListAdapter.OnSpeakerClickListener onSpeakerClickListener;

    public SpeakersRecyclerWithListAdapter(Context context, OnSpeakerClickListener onSpeakerClickListener) {
        this.context = context;
        this.onSpeakerClickListener = onSpeakerClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView speakerName;

        SpeakersRecyclerWithListAdapter.OnSpeakerClickListener onSpeakerClickListener;

        ViewHolder(View itemView, SpeakersRecyclerWithListAdapter.OnSpeakerClickListener onSpeakerClickListener) {
            super(itemView);

            speakerName = itemView.findViewById(R.id.locationName);

            this.onSpeakerClickListener = onSpeakerClickListener;

            itemView.setOnClickListener(this);
        }

        void bindDataSet(Speaker speaker) {
            speakerName.setText(speaker.getName());
        }

        @Override
        public void onClick(View v) {
            onSpeakerClickListener.onSpeakerClick(getAdapterPosition());

            //bring the parcelable data from the clicked object
            Intent intent = new Intent(context, SpeakersInfoPopUp.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Speaker Info", dataSet.get(getAdapterPosition()));

            context.startActivity(intent);

        }
    }

    @NonNull
    @Override
    public SpeakersRecyclerWithListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.recycler_view_speaker_item, parent, false);
        return new ViewHolder(view, onSpeakerClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SpeakersRecyclerWithListAdapter.ViewHolder holder, int position) {
        if (dataSet != null) {
            holder.bindDataSet(dataSet.get(position));
        }

    }

    @Override
    public int getItemCount() {
        if (dataSet != null) {
            return dataSet.size();
        } else {
            return 0;
        }
    }

    public void changeDataSet(List<Speaker> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    /**
     * Sets up an interface to interact with onClick events
     */
    public interface OnSpeakerClickListener {
        void onSpeakerClick(int position);
    }

}
