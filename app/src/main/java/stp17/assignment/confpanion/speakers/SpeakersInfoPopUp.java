package stp17.assignment.confpanion.speakers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.locations.Location;

/**
 * The popup screen the users sees when a session is clicked,
 * the data is being brought in through intents using parcelable data
 *
 * @Author Stephen Preece
 * @Date 27/11/2019
 */


public class SpeakersInfoPopUp extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.speakerinfopopup);

        //set text view variables to XML text views
        ImageView speakerImage = findViewById(R.id.speakerImage);
        TextView speakerDescTextView = findViewById(R.id.speakerDesc);
        TextView speakerTwitterTextView = findViewById(R.id.speakerTwitter);
        TextView speakerIdTextView = findViewById(R.id.speakerId);


        //unpack parcel and create new Session object using passed parcelable data
        Intent intent = getIntent();
        Speaker speakerInfo = intent.getParcelableExtra("Speaker Info");

        //set the session info variables
        String speakerDesc = speakerInfo.getBiography();
        String speakerId = speakerInfo.getId();
        String speakerTwitter = speakerInfo.getTwitter();
        String imageName = speakerInfo.getId().toLowerCase().trim();

        //bind data to the text views
        speakerDescTextView.setText(String.valueOf(speakerDesc));
        speakerIdTextView.setText(speakerId);
        speakerTwitterTextView.setText(String.valueOf(speakerTwitter));

        //retrieve the file name of the image based of the iD, since all file names are same as iDs
        int resID = getResources().getIdentifier(imageName, "drawable", getPackageName());
        speakerImage.setImageResource(resID);


        // set the activity screen size
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .55));
    }
}
