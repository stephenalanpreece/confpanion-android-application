package stp17.assignment.confpanion.Favourites;

import java.util.ArrayList;

import stp17.assignment.confpanion.sessions.Session;


/**
 * Singleton class to init a global array list to save favourited sessions
 * @Author Stephen Preece
 * @Date 30/11/2019
 * @Version 1
 */
public class FavouriteList {

    private static FavouriteList mInstance;
    private ArrayList<Session> list;

    public static FavouriteList getInstance() {
        if (mInstance == null)
            mInstance = new FavouriteList();

        return mInstance;
    }

    private FavouriteList() {
        list = new ArrayList<>();
    }

    // retrieve array from anywhere
    public ArrayList<Session> getArray() {
        return this.list;
    }

    //Add element to array
    public void addToArray(Session session) {
        list.add(session);
    }

    //Remove element from array
    public void removeFromArray(Session session) {
        list.remove(session);
    }

}
