package stp17.assignment.confpanion.Favourites;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;

/**
 * A basic adapter class that uses a global array-list of saved sessions to display
 * in the favourites fragment IMPORTANT: Did not comment each method as it's template code
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */
public class FavouritesRecyclerWithListAdapter extends RecyclerView.Adapter<FavouritesRecyclerWithListAdapter.ViewHolder> {

    private ArrayList<Session> favouriteList;
    private Context mContext;
    private OnFavouriteClickListener onFavouriteClickListener;

    public FavouritesRecyclerWithListAdapter(ArrayList<Session> favouriteList, Context mContext, OnFavouriteClickListener onFavouriteClickListener) {
        this.favouriteList = favouriteList;
        this.mContext = mContext;
        this.onFavouriteClickListener = onFavouriteClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_view_favourite_item, parent, false);
        ViewHolder holder = new ViewHolder(view, onFavouriteClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (favouriteList != null) {
            holder.bindDataSet(favouriteList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (favouriteList != null) {
            return favouriteList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView favouriteSessionTitle;
        TextView favouriteSessionTime;
        TextView favouriteSessionSpeaker;
        ConstraintLayout parentLayout;

        OnFavouriteClickListener onFavouriteClickListener;

        public ViewHolder(@NonNull View itemView, OnFavouriteClickListener onFavouriteClickListener) {
            super(itemView);

            favouriteSessionSpeaker = itemView.findViewById(R.id.favouriteSessionSpeaker);
            favouriteSessionTitle = itemView.findViewById(R.id.favouriteSessionTitle);
            favouriteSessionTime = itemView.findViewById(R.id.favouriteSessionTime);
            parentLayout = itemView.findViewById(R.id.fav_layout);
            this.onFavouriteClickListener = onFavouriteClickListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onFavouriteClickListener.onFavouriteClick(getAdapterPosition());

            //bring the parcelable data from the clicked object
            Intent intent = new Intent(mContext, FavouriteSessionInfoPopUp.class);
            intent.putExtra("Session Info", favouriteList.get(getAdapterPosition()));

            mContext.startActivity(intent);
        }

        public void bindDataSet(Session session) {
            favouriteSessionTime.setText(session.getTimeStart());
            favouriteSessionTitle.setText(session.getTitle());
            favouriteSessionSpeaker.setText(session.getSpeakerId());
        }

    }

    public void changeDataSet(ArrayList<Session> favouriteList) {
        this.favouriteList = favouriteList;
        notifyDataSetChanged();
    }


    /**
     * Sets up an interface to interact with onClick events
     */
    public interface OnFavouriteClickListener {
        void onFavouriteClick(int position);
    }
}

