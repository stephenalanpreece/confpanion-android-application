package stp17.assignment.confpanion.Favourites;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;

/**
 * The popup screen the users sees when a favourited session is clicked,
 * the data is being brought in through intents using parcelable data
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

public class FavouriteSessionInfoPopUp extends Activity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.favouritesessioninfopopup);

        //set text view variables to XML text views
        TextView sessionLocationTextView = findViewById(R.id.textView7);
        TextView sessionNameTextView = findViewById(R.id.textView5);
        TextView sessionSpeakerTextView = findViewById(R.id.textView6);
        TextView sessionTimeTextView = findViewById(R.id.textView10);
        TextView sessionDescTextView = findViewById(R.id.textView9);
        TextView sessionTimeEndTextView = findViewById(R.id.timeEnd);

        Button favouriteButton = findViewById(R.id.favourite_button);

        //unpack parcel and create new Session object using passed parcelable data
        Intent intent = getIntent();
        Session sessionInfo = intent.getParcelableExtra("Session Info");

        //set the session info variables
        String sessionTitle = sessionInfo.getTitle();
        String sessionSpeaker = sessionInfo.getSpeakerId();
        String sessionDesc = sessionInfo.getContent();
        String sessionTime = sessionInfo.getTimeStart();
        String sessionLocation = sessionInfo.getLocationId();
        String sessionType = sessionInfo.getSessionType();
        String sessionTimeEnd = sessionInfo.getTimeEnd();

        //bind data to the text views
        sessionLocationTextView.setText(sessionLocation);
        sessionNameTextView.setText(sessionTitle);
        sessionTimeTextView.setText(sessionTime);
        sessionSpeakerTextView.setText(sessionSpeaker);
        sessionDescTextView.setText(sessionDesc);
        sessionTimeEndTextView.setText(sessionTimeEnd);


        //in the event of the session not having a speaker, for example with a social, "no speaker" will be displayed to the user
        if (sessionSpeakerTextView.getText().length() == 0)
            sessionSpeakerTextView.setText("No Speaker");

        //checks to see which session is of declared type and removes button as they are not able to favourite them
        if (sessionType.contains("registration") || sessionType.contains(
                "coffee") || sessionType.contains("lunch") || sessionType.contains("dinner"))
            favouriteButton.setVisibility(View.GONE);


        // set the activity screen size
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .55));
    }

    //receives the intent from the previous onclick and uses that to save a new favourite using the singleton array-list
    public void addFavourite(View view) {
        Intent favourite = getIntent();
        Session newFavourite = favourite.getParcelableExtra("Session Info");
        FavouriteList.getInstance().addToArray(newFavourite);
    }

    //TODO Fix this!!
    public void removeFavourite(View view) {
        Intent favourite = getIntent();
        Session newFavourite = favourite.getParcelableExtra("Session Info");
        FavouriteList.getInstance().removeFromArray(newFavourite);
        FavouriteList.getInstance().getArray();
    }

}
