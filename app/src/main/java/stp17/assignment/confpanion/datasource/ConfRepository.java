package stp17.assignment.confpanion.datasource;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import stp17.assignment.confpanion.locations.Location;
import stp17.assignment.confpanion.locations.LocationDao;
import stp17.assignment.confpanion.sessions.Session;
import stp17.assignment.confpanion.sessions.SessionDao;
import stp17.assignment.confpanion.speakers.Speaker;
import stp17.assignment.confpanion.speakers.SpeakerDao;

/**
 * A repository that acts as a layer of abstraction between the Dao and the application
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

public class ConfRepository {
    private SessionDao sessionDao;
    private LocationDao locationDao;
    private SpeakerDao speakerDao;

    public ConfRepository(Application application) {
        ConfRoomDatabase db = ConfRoomDatabase.getDatabase(application);
        sessionDao = db.getSessionDao();
        locationDao = db.getLocationDao();
        speakerDao = db.getSpeakerDao();
    }


    //queries the dao and returns the results to live data
    public LiveData<List<Session>> getAllSessions() {
        return sessionDao.getAllSessions();
    }

    public LiveData<List<Session>> getAllSessionsFromFriday() {
        return sessionDao.getAllFridaySessions();
    }

    public LiveData<List<Session>> getAllSessionsFromThursday() {
        return sessionDao.getAllThursdaySessions();
    }

    public LiveData<List<Session>> getAllSessionsFromTuesday() {
        return sessionDao.getAllTuesdaySessions();
    }

    public LiveData<List<Session>> getAllSessionsFromWednesday() {
        return sessionDao.getAllWednesdaySessions();
    }

    public LiveData<List<Location>> getAllLocations() {
        return locationDao.getAllLocations();
    }

    public LiveData<List<Speaker>> getAllSpeakers() {
        return speakerDao.getAllSpeakers();
    }

}
