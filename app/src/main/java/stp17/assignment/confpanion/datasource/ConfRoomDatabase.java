package stp17.assignment.confpanion.datasource;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import stp17.assignment.confpanion.locations.Location;
import stp17.assignment.confpanion.locations.LocationDao;
import stp17.assignment.confpanion.sessions.Session;
import stp17.assignment.confpanion.sessions.SessionDao;
import stp17.assignment.confpanion.speakers.Speaker;
import stp17.assignment.confpanion.speakers.SpeakerDao;

/**
 * A Room database class that fetches the pre-defined conf database
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

@Database(entities = {Session.class, Location.class, Speaker.class}, version = 1)
public abstract class ConfRoomDatabase extends RoomDatabase {
    private static ConfRoomDatabase INSTANCE;
    private static Context myContext;
    private static final String DB_NAME = "conf.db";

    public abstract SessionDao getSessionDao();

    public abstract LocationDao getLocationDao();

    public abstract SpeakerDao getSpeakerDao();

    public static ConfRoomDatabase getDatabase(final Context context) {
        // We use the Singleton design pattern so that we only every have one instance of the Room database
        myContext = context;

        if (INSTANCE == null) {
            synchronized (ConfRoomDatabase.class) {
                if (INSTANCE == null) {
                    // For simplicity and to show it's possible, I've added support to allow queries on the
                    // main thread. It is bad practice to allow queries on the main thread since
                    // it can reduce UI performance. Normally, create an AsyncTask or use LiveData
                    //INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    //FaaRoomDatabase.class, DB_NAME).allowMainThreadQueries().addCallback(sRoomDatabaseCallback).build();
                    INSTANCE = createDatabase(context);
                    /* Do the following when migrating to a new version of the database
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FaaRoomDatabase.class, "faa_database").addMigrations(MIGRATION_1_2, MIGRATION_2_3).build();
                    */
                }
            }
        }
        return INSTANCE;
    }

    private static ConfRoomDatabase createDatabase(Context context) {
        RoomDatabase.Builder<ConfRoomDatabase> builder =
                Room.databaseBuilder(context.getApplicationContext(), ConfRoomDatabase.class,
                        DB_NAME);

        // The AssetSQLiteOpenHelperFactory does the heavy lifting of the database file
        // from assets/databases to our /data/data/package-name/databases folder
        // It is important that the Room table
        return (Room.databaseBuilder(context.getApplicationContext(), ConfRoomDatabase.class, DB_NAME)
                .createFromAsset("databases/conf.db")
                .build());

    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            Log.d("migrate", "Doing a migrate from version 1 to 2");
            // This is where we make relevant database data changes,
            // or copy data from old table to a new table.
            // Deals with the migration from version 1 to version 2
            // I MANAGED TO AVOID HAVING TO ADD THIS, HENCE CMENTED OUT
            // IT SEEMED THOUGH THAT FOR THIS EXAMPLE ROOM REQUIRED A CHANGE
            // OF VERSION AND A CALL TO THIS MIGRATION EVEN THOUGH IT DOESN'T
            // DO ANYTHING.
            /*String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS 'cats' (" +
                    "'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " 'name' TEXT," +
                    " 'gender' TEXT," +
                    " 'breed' TEXT," +
                    " 'description' TEXT," +
                    " 'dob' INTEGER, " +
                    " 'admission_date' INTEGER," +
                    " 'main_image_path' TEXT)" ;

            database.execSQL(SQL_CREATE_TABLE);*/
        }
    };

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }


            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final SessionDao sessionDao;
        private final LocationDao locationDao;
        private final SpeakerDao speakerDao;

        PopulateDbAsync(ConfRoomDatabase db) {

            sessionDao = db.getSessionDao();
            locationDao = db.getLocationDao();
            speakerDao = db.getSpeakerDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }
}