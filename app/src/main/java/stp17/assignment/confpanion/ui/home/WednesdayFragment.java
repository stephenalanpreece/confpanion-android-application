package stp17.assignment.confpanion.ui.home;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;
import stp17.assignment.confpanion.sessions.SessionViewModel;
import stp17.assignment.confpanion.sessions.SessionsRecyclerWithListAdapter;

/**
 * A simple {@link Fragment} subclass, this one is for Wednesday of the conference and will
 * contain each event happening on that day
 */
public class WednesdayFragment extends Fragment implements SessionsRecyclerWithListAdapter.OnSessionClickListener {
    private SessionsRecyclerWithListAdapter sessionsRecyclerAdapter;
    private SessionViewModel sessionViewModel;


    public WednesdayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_wednesday, container, false);

        sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel.class);
        sessionsRecyclerAdapter = sessionViewModel.getAdapter();
        if (sessionsRecyclerAdapter == null) {
            sessionsRecyclerAdapter = new SessionsRecyclerWithListAdapter(getContext(), this);
            sessionViewModel.setAdapter(sessionsRecyclerAdapter);
        }
        RecyclerView listSessions = view.findViewById(R.id.session_list);
        listSessions.setAdapter(sessionsRecyclerAdapter);

        //load in current favourites
        loadSessions();

        listSessions.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }

    //load in the sessions for wednesday
    private void loadSessions() {
        LiveData<List<Session>> sessionList = sessionViewModel.loadSessionsWednesday();
        sessionList.observe(this, sessions -> sessionsRecyclerAdapter.changeDataSet(sessions));
    }

    @Override
    public void onSessionClick(int position) {

    }
}
