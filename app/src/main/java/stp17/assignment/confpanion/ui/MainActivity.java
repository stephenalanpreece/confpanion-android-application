package stp17.assignment.confpanion.ui;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import stp17.assignment.confpanion.Favourites.FavouriteList;
import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;
import stp17.assignment.confpanion.sessions.SessionInfoPopUp;
import stp17.assignment.confpanion.ui.home.FavouritesFragment;
import stp17.assignment.confpanion.ui.home.FridayFragment;
import stp17.assignment.confpanion.ui.home.ThursdayFragment;
import stp17.assignment.confpanion.ui.home.TuesdayFragment;
import stp17.assignment.confpanion.ui.home.WednesdayFragment;


/**
 * The Main Activity, sets up the UI, and uses fragments for each day as well as favourites to display content
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

public class MainActivity extends AppCompatActivity {
    ArrayList<Session> savedSessions = FavouriteList.getInstance().getArray();

    private static final int TUESDAY_TAB = 0;
    private static final int WEDNESDAY_TAB = 1;
    private static final int THURSDAY_TAB = 2;
    private static final int FRIDAY_TAB = 3;
    private static final int FAV_TAB = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Set up the toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Set up the drawer menu
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle =
                new ActionBarDrawerToggle(this,
                        drawer,
                        toolbar,
                        R.string.nav_open_drawer,
                        R.string.nav_close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        //attach the adapter to viewpager
        SectionsPagerAdapter pagerAdapter = new
                SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        //Set up the tab layout
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        loadData();

        pager.addOnPageChangeListener(addPageChangeListener());
    }

    private ViewPager.OnPageChangeListener addPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case TUESDAY_TAB:
                    case WEDNESDAY_TAB:
                    case THURSDAY_TAB:
                    case FRIDAY_TAB:
                    case FAV_TAB:
                        loadData();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
    }


    //inner class for fragments
    private class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //creates a new fragment for each tab when scrolled or clicked to it
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    loadData();
                    return new TuesdayFragment();
                case 1:
                    loadData();
                    return new WednesdayFragment();
                case 2:
                    loadData();
                    return new ThursdayFragment();
                case 3:
                    loadData();
                    return new FridayFragment();
                case 4:
                    loadData();
                    return new FavouritesFragment();
            }
            return null;
        }

        //sets the day of the week for each tab
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getText(R.string.tuesday_tab);
                case 1:
                    return getText(R.string.wednesday_tab);
                case 2:
                    return getText(R.string.thursday_tab);
                case 3:
                    return getText(R.string.friday_tab);
                case 4:
                    return getText(R.string.favourites_tab);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }

    //Opens the locations activity
    public void openLocations(MenuItem item) {
        Intent openLocations = new Intent(this, Locations.class);
        startActivity(openLocations);
    }

    //Opens the speakers activity
    public void openSpeakers(MenuItem item) {
        Intent openSpeakers = new Intent(this, Speakers.class);
        startActivity(openSpeakers);
    }


    //Sets up intent to view conference page
    public void viewBrowser(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.iosdevuk.com/"));
        startActivity(intent);
    }

    //loads saved preferences using gson and json then sets it to the saved sessions arraylist
    public void loadData() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "sharedPreferences",
                MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Favourite List", null);
        Type type = new TypeToken<ArrayList<Session>>() {
        }.getType();
        savedSessions = gson.fromJson(json, type);

    }

}
