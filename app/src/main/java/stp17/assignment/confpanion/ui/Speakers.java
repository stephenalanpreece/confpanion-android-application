package stp17.assignment.confpanion.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.speakers.Speaker;
import stp17.assignment.confpanion.speakers.SpeakerViewModel;
import stp17.assignment.confpanion.speakers.SpeakersRecyclerWithListAdapter;

/**
 * The speakers activity
 * Will display all the different speakers present at the conference
 *
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

public class Speakers extends AppCompatActivity implements SpeakersRecyclerWithListAdapter.OnSpeakerClickListener {
    private SpeakersRecyclerWithListAdapter speakerRecyclerAdapter;
    private SpeakerViewModel speakerViewModel;
    private static Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakers);

        Speakers.SectionsPagerAdapter pagerAdapter = new Speakers.SectionsPagerAdapter(
                getSupportFragmentManager());

        //Set up the toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        //Set up the drawer menu
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle =
                new ActionBarDrawerToggle(this,
                        drawer,
                        toolbar,
                        R.string.nav_open_drawer,
                        R.string.nav_close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        speakerViewModel = ViewModelProviders.of(this).get(SpeakerViewModel.class);
        speakerRecyclerAdapter = speakerViewModel.getAdapter();
        if (speakerRecyclerAdapter == null) {
            speakerRecyclerAdapter = new SpeakersRecyclerWithListAdapter(getApplicationContext(),
                    this);
            speakerViewModel.setAdapter(speakerRecyclerAdapter);
        }

        RecyclerView listSpeakers = this.findViewById(R.id.speaker_list);
        listSpeakers.setAdapter(speakerRecyclerAdapter);

        loadSpeakers();

        listSpeakers.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void loadSpeakers() {
        LiveData<List<Speaker>> speakerList = speakerViewModel.getSpeakerList();
        speakerList.observe(this, speakers -> speakerRecyclerAdapter.changeDataSet(speakers));
    }

    @Override
    public void onSpeakerClick(int position) {

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return 0;
        }
    }

    //Sets up intent to view conference page
    public void viewBrowser(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.iosdevuk.com/"));
        startActivity(intent);
    }

    //Opens the main activity
    public void openHome(MenuItem item) {
        Intent openHome = new Intent(this, MainActivity.class);
        startActivity(openHome);
    }

    //Opens the locations activity
    public void openLocation(MenuItem item) {
        Intent openLocation = new Intent(this, Locations.class);
        startActivity(openLocation);
    }

    public static Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }


}
