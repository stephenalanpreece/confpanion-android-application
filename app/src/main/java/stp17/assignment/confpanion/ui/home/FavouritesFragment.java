package stp17.assignment.confpanion.ui.home;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import stp17.assignment.confpanion.Favourites.FavouriteList;
import stp17.assignment.confpanion.Favourites.FavouritesRecyclerWithListAdapter;
import stp17.assignment.confpanion.R;
import stp17.assignment.confpanion.sessions.Session;
import stp17.assignment.confpanion.ui.MainActivity;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} class, this one will store all the users saved favourites
 */
public class FavouritesFragment extends Fragment implements FavouritesRecyclerWithListAdapter.OnFavouriteClickListener {
    ArrayList<Session> savedSessions = FavouriteList.getInstance().getArray();

    public FavouritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_favourites, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.favourite_session_list);
        FavouritesRecyclerWithListAdapter adapter = new FavouritesRecyclerWithListAdapter(
                savedSessions, getContext(), this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        //load in current favourites
        loadData();

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onFavouriteClick(int position) {

    }


    //loads saved preferences using gson and json then sets it to the saved sessions arraylist
    public void loadData() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(
                "sharedPreferences",
                MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Favourite List", null);
        Type type = new TypeToken<ArrayList<Session>>() {
        }.getType();

        savedSessions = gson.fromJson(json, type); }
}
