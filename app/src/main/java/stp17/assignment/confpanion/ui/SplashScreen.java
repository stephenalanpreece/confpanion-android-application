package stp17.assignment.confpanion.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import stp17.assignment.confpanion.R;

/**
 * Simple splash screen that the user will be presented with on entry to the app
 * Simply displays the app title and a loading image
 *
 * @author Stephen Preece
 * @version 1
 * @since 06-11-2019
 */

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(
                () -> startActivity(new Intent(getApplicationContext(), MainActivity.class)), 1 * 1500);// 1.5 Second delay
    }
}
