package stp17.assignment.confpanion.sessions;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;


/**
 * A parcelable class for each session used at the conference
 * Also acts as an entity for the relevant table
 *
 * @Author Stephen Preece
 * @version 1
 * @Date 19/11/2019
 */

@Entity(tableName = "sessions")
public class Session implements Parcelable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String dbId;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "locationId")
    private String locationId;
    @ColumnInfo(name = "sessionDate")
    private String sessionDate;
    @ColumnInfo(name = "sessionOrder")
    private int sessionOrder;
    @ColumnInfo(name = "timeStart")
    private String timeStart;
    @ColumnInfo(name = "timeEnd")
    private String timeEnd;
    @ColumnInfo(name = "sessionType")
    private String sessionType;
    @ColumnInfo(name = "speakerId")
    private String speakerId;

    public Session(String dbId, String title, String content, String locationId, String sessionDate, int sessionOrder, String timeStart, String timeEnd, String sessionType, String speakerId) {
        this.dbId = dbId;
        this.title = title;
        this.content = content;
        this.locationId = locationId;
        this.sessionDate = sessionDate;
        this.sessionOrder = sessionOrder;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.sessionType = sessionType;
        this.speakerId = speakerId;
    }


    protected Session(Parcel in) {
        dbId = in.readString();
        title = in.readString();
        content = in.readString();
        locationId = in.readString();
        sessionDate = in.readString();
        sessionOrder = in.readInt();
        timeStart = in.readString();
        timeEnd = in.readString();
        sessionType = in.readString();
        speakerId = in.readString();
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        @Override
        public Session createFromParcel(Parcel in) {
            return new Session(in);
        }

        @Override
        public Session[] newArray(int size) {
            return new Session[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public int getSessionOrder() {
        return sessionOrder;
    }

    public void setSessionOrder(int sessionOrder) {
        this.sessionOrder = sessionOrder;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getSpeakerId() {
        return speakerId;
    }

    public void setSpeakerId(String speakerId) {
        this.speakerId = speakerId;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        this.dbId = dbId;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "session id='" + dbId + '\'' +
                ", session title=" + title +
                ", session content='" + content + '\'' +
                ", location name='" + locationId + '\'' +
                ", session date=" + sessionDate +
                ", session order=" + sessionOrder +
                ", start time='" + timeStart + '\'' +
                ", end time='" + timeEnd + '\'' +
                ", session type='" + sessionType + '\'' +
                ", speaker id='" + speakerId + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbId);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(locationId);
        dest.writeString(sessionDate);
        dest.writeInt(sessionOrder);
        dest.writeString(timeStart);
        dest.writeString(timeEnd);
        dest.writeString(sessionType);
        dest.writeString(speakerId);
    }
}
