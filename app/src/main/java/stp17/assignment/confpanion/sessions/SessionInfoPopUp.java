package stp17.assignment.confpanion.sessions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;


import com.google.gson.Gson;
import stp17.assignment.confpanion.R;

import stp17.assignment.confpanion.Favourites.FavouriteList;

/**
 * The popup screen the users sees when a session is clicked,
 * the data is being brought in through intents using parcelable data
 *
 * @Author Stephen Preece
 * @Date 27/11/2019
 */


public class SessionInfoPopUp extends Activity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.sessioninfopopup);

        //set text view variables to XML text views
        TextView sessionLocationTextView = findViewById(R.id.textView7);
        TextView sessionLocationTimeEndTextView = findViewById(R.id.timeEnd);
        TextView sessionNameTextView = findViewById(R.id.textView5);
        TextView sessionSpeakerTextView = findViewById(R.id.textView6);
        TextView sessionTimeTextView = findViewById(R.id.textView10);
        TextView sessionDescTextView = findViewById(R.id.textView9);
        Button favouriteButton = findViewById(R.id.favourite_button);

        //unpack parcel and create new Session object using passed parcelable data
        Intent intent = getIntent();
        Session sessionInfo = intent.getParcelableExtra("Session Info");

        //set the session info variables
        String sessionTitle = sessionInfo.getTitle();
        String sessionSpeaker = sessionInfo.getSpeakerId();
        String sessionDesc = sessionInfo.getContent();
        String sessionTime = sessionInfo.getTimeStart();
        String sessionLocation = sessionInfo.getLocationId();
        String sessionType = sessionInfo.getSessionType();
        String sessionTimeEnd = sessionInfo.getTimeEnd();

        //bind data to the text views
        sessionLocationTextView.setText(sessionLocation);
        sessionNameTextView.setText(sessionTitle);
        sessionTimeTextView.setText(sessionTime);
        sessionSpeakerTextView.setText(sessionSpeaker);
        sessionDescTextView.setText(sessionDesc);
        sessionLocationTimeEndTextView.setText(sessionTimeEnd);

        //in the event of the session not having a speaker, for example with a social, "no speaker" will be displayed to the user
        if (sessionSpeakerTextView.getText().length() == 0)
            sessionSpeakerTextView.setText("No Speaker");

        //checks to see which session is of declared type and removes button as they are not able to favourite them
        if (sessionType.contains("registration") || sessionType.contains(
                "coffee") || sessionType.contains("lunch") || sessionType.contains("dinner"))
            favouriteButton.setVisibility(View.GONE);


        // set the activity screen size
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .55));
    }

    //on destroy, saves current state of favourites
    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveData();
    }

    //creates a new favourite based on the current passed intent and saves to singleton class arraylist
    public void addFavourite(View view) {
        Intent favourite = getIntent();
        Session newFavourite = favourite.getParcelableExtra("Session Info");
        FavouriteList.getInstance().addToArray(newFavourite);
    }

    //saves the favourited list to sharedprefs to be retrieved again after app has been destroyed
    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPreferences",
                MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(FavouriteList.getInstance().getArray());
        editor.putString("Favourite List", json);
        editor.apply();
    }
}
