package stp17.assignment.confpanion.sessions;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import stp17.assignment.confpanion.datasource.ConfRepository;

/**
 * A View Model that handles the session data that needs to be stored in the UI
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

public class SessionViewModel extends AndroidViewModel {

    private ConfRepository repository;
    private LiveData<List<Session>> sessionList;

    private String sessionTime;
    private String sessionTitle;
    private String sessionSpeaker;
    private String sessionDesc;
    private String sessionLocation;

    private SessionsRecyclerWithListAdapter adapter;

    public SessionViewModel(@NonNull Application application) {
        super(application);
        repository = new ConfRepository(application);

        loadSessions();
    }

    public LiveData<List<Session>> getSessions() {
        return sessionList;
    }

    public SessionsRecyclerWithListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(SessionsRecyclerWithListAdapter adapter) {
        this.adapter = adapter;
    }

    private void loadSessions() {
        sessionList = repository.getAllSessions();
    }

    public LiveData<List<Session>> loadSessionsTuesday() {
        sessionList = repository.getAllSessionsFromTuesday();
        return sessionList;
    }

    public LiveData<List<Session>> loadSessionsWednesday() {
        sessionList = repository.getAllSessionsFromWednesday();
        return sessionList;
    }

    public LiveData<List<Session>> loadSessionsThursday() {
        sessionList = repository.getAllSessionsFromThursday();
        return sessionList;
    }

    public LiveData<List<Session>> loadSessionsFriday() {
        sessionList = repository.getAllSessionsFromFriday();
        return sessionList;
    }


    public ConfRepository getRepository() {
        return repository;
    }

    public void setRepository(ConfRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<Session>> getSessionList() {
        return sessionList;
    }

    public void setSessionList(LiveData<List<Session>> sessionList) {
        this.sessionList = sessionList;
    }

    public String getSessionDesc() {
        return sessionDesc;
    }

    public void setSessionDesc(String sessionDesc) {
        this.sessionDesc = sessionDesc;
    }

    public String getSessionLocation() {
        return sessionLocation;
    }

    public void setSessionLocation(String sessionSLocation) {
        this.sessionLocation = sessionSLocation;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getSessionTitle() {
        return sessionTitle;
    }

    public void setSessionTitle(String sessionTitle) {
        this.sessionTitle = sessionTitle;
    }

    public String getSessionSpeaker() {
        return sessionSpeaker;
    }

    public void setSessionSpeaker(String sessionSpeaker) {
        this.sessionSpeaker = sessionSpeaker;
    }

}
