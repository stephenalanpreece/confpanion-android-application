package stp17.assignment.confpanion.sessions;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

/**
 *
 * A data access object for the sessions table
 * @Author Stephen Preece
 * @Version 1
 * @Date 19/11/2019
 */

@Dao
public interface SessionDao {
    @Query("SELECT * FROM sessions")
    LiveData<List<Session>> getAllSessions();

    @Query ("SELECT * FROM sessions")
    List<Session> getAllSessionsSync();

    @Query("SELECT * FROM sessions WHERE sessionDate = '2019-12-10' ORDER BY sessionOrder ASC")
    LiveData<List<Session>> getAllTuesdaySessions();

    @Query("SELECT * FROM sessions WHERE sessionDate = '2019-12-11' ORDER BY sessionOrder ASC")
    LiveData<List<Session>> getAllWednesdaySessions();

    @Query("SELECT * FROM sessions WHERE sessionDate = '2019-12-12' ORDER BY sessionOrder ASC")
    LiveData<List<Session>> getAllThursdaySessions();

    @Query("SELECT * FROM sessions WHERE sessionDate = '2019-12-13' ORDER BY sessionOrder ASC")
    LiveData<List<Session>> getAllFridaySessions();
}
