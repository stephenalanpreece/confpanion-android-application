package stp17.assignment.confpanion.sessions;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import stp17.assignment.confpanion.R;

/**
 * A basic adapter class that is able to use Livedata to be fed into the UI
 * This one takes Sessions IMPORTANT: Did not comment each method as it's template code
 *
 * @Author Stephen Preece
 * @Date 19/11/2019
 */

public class SessionsRecyclerWithListAdapter extends RecyclerView.Adapter<SessionsRecyclerWithListAdapter.ViewHolder> {
    private final Context context;
    private List<Session> dataSet;
    private OnSessionClickListener onSessionClickListener;

    public SessionsRecyclerWithListAdapter(Context context, OnSessionClickListener onSessionClickListener) {
        this.context = context;
        this.onSessionClickListener = onSessionClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView sessionTime;
        TextView sessionTitle;
        TextView sessionSpeaker;

        OnSessionClickListener onSessionClickListener;

        ViewHolder(View itemView, OnSessionClickListener onSessionClickListener) {
            super(itemView);

            sessionTime = itemView.findViewById(R.id.sessionTime);
            sessionTitle = itemView.findViewById(R.id.sessionTitle);
            sessionSpeaker = itemView.findViewById(R.id.sessionSpeaker);
            this.onSessionClickListener = onSessionClickListener;

            itemView.setOnClickListener(this);
        }

        void bindDataSet(Session session) {
            sessionTime.setText(session.getTimeStart());
            sessionTitle.setText(session.getTitle());
            sessionSpeaker.setText(session.getSpeakerId());
        }

        @Override
        public void onClick(View v) {
            onSessionClickListener.onSessionClick(getAdapterPosition());

            //bring the parcelable data from the clicked object
            Intent intent = new Intent (context, SessionInfoPopUp.class);
            intent.putExtra("Session Info", dataSet.get(getAdapterPosition()));

            context.startActivity(intent);
        }
    }

    @NonNull
    @Override
    public SessionsRecyclerWithListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.recycler_view_session_item, parent, false);
        return new ViewHolder(view, onSessionClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SessionsRecyclerWithListAdapter.ViewHolder holder, int position) {
        if (dataSet != null) {
            holder.bindDataSet(dataSet.get(position));
        }

    }

    @Override
    public int getItemCount() {
        if (dataSet != null) {
            return dataSet.size();
        } else {
            return 0;
        }
    }

    public void changeDataSet(List<Session> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    /**
     * Sets up an interface to interact with onClick events
     */
    public interface OnSessionClickListener {
        void onSessionClick(int position);
    }

}
